import { API_props, API_config, API_fetch_param, receivedData, actionHandlerFunction } from './types';
declare class CAPI {
    __props: API_props;
    __config: API_config;
    get isAuthenticated(): boolean;
    get isAuthorized(): boolean;
    setConfig(config: Partial<API_config>): void;
    setActionHandler(action: string, callback: actionHandlerFunction): void;
    authenticateWithName(serviceName: string): Promise<void>;
    authorizeWithKey(serviceKey: string): Promise<void>;
    get<T = any>(param: API_fetch_param): Promise<receivedData<T>>;
    post<T = any>(param: API_fetch_param): Promise<receivedData<T>>;
    __fetch<T>(param: API_fetch_param): Promise<receivedData<T>>;
    __execAction<T = any>(actionName: string, data: T): T;
    __setAccessKey(accessKey: string): string;
    connectionClose(): void;
}
export default CAPI;
//# sourceMappingURL=renette-api.d.ts.map