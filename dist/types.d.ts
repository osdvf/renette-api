export interface API_fetch_param {
    resource: string;
    action: string;
    method?: 'GET' | 'POST';
    headers?: Headers;
    data?: object;
    id?: number;
}
export interface sentMessage {
    type: 'error' | 'warning' | 'success';
    html: string;
    recoverable?: true;
}
export type API_config = {
    apiUri: string;
    cors: boolean;
    allowedMethod: string[];
    defaultHeaders: Headers;
    actionHandlers: {
        [a: string]: actionHandlerFunction[];
    };
};
export type API_props = {
    preparing: Promise<any> | false;
    authenticated: boolean;
    authorized: boolean;
    accessKey: string;
};
export type actionHandlerFunction = (res: receivedData<any>) => receivedData<any> | false;
export type receivedData<t> = {
    action?: string[];
    code?: number;
    data: t;
};
export type receivedAccessKey = {
    accessKey: string;
};
//# sourceMappingURL=types.d.ts.map